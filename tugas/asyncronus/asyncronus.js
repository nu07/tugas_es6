let content = document.getElementById("konten");

let api_url = "https://edi-server.herokuapp.com/posts";

//get
async function tampil() {
  await axios
    .get(api_url)
    .then((res) => {
      res.data.map((hasil) => {
        content.innerHTML += `<tr>
          
       <th scope="row" id="nomer" >${hasil.id}</th>
       <td id="title">${hasil.title}</td>
       <td id="content">${hasil.content}</td>
       <td> 
       <button type="button" class="btn btn-danger " id="hapusid" data-index="${hasil.id}"data-bs-toggle="modal" data-bs-target="#delete" >Hapus</button>
       <button type="button" class="btn btn-warning fullstuck" id="editid" data-index="${hasil.id}"  data-bs-toggle="modal" data-bs-target="#edit">Edit</button>
       </td>
       </tr>
    
    
       `;
      });
    })
    .catch((err) => console.log(err));
}
tampil();

const modalDelete = document.getElementById("delete");
modalDelete.addEventListener("shown.bs.modal", (e) => {
  const btn = e.relatedTarget;
  const nilai = btn.dataset.index;
  //   console.log(nilai);
  document
    .getElementById("hapusdata")
    .addEventListener("click", () => hapus(nilai));
});

async function hapus(nilai) {
  let titlee = document.querySelector("#edittitle").value;
  let contentt = document.querySelector("#editcontent").value;

  await axios
    .delete(`https://edi-server.herokuapp.com/posts/${nilai}`, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di Edit");
    })
    .then(function (response) {
      location.reload();
    })
    .then((response) => console.log(response))
    .catch(function (error) {
      console.log(error);
    });
}

const modalEdit = document.getElementById("edit");
modalEdit.addEventListener("shown.bs.modal", (e) => {
  const btn = e.relatedTarget;
  const nilai = btn.dataset.index;
  document.getElementById("ubahdata").addEventListener("click", function () {
    edit(nilai);
  });
});

async function edit(nilai) {
  let titlee = document.querySelector("#edittitle").value;
  let contentt = document.querySelector("#editcontent").value;

  await axios
    .put(`https://edi-server.herokuapp.com/posts/${nilai}`, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di Edit");
    })
    .then(function (response) {
      location.reload();
    })
    .catch(function (error) {
      console.log(error);
    });
}

async function tambah() {
  let titlee = document.querySelector("#formTitle").value;
  let contentt = document.querySelector("#formContent").value;

  await axios
    .post(api_url, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di tambahkan");
    })
    .then(function (response) {
      location.reload();
    })
    .catch(function (error) {
      console.log(error);
    });
}
