let content = document.getElementById("konten");

let api_url = "https://edi-server.herokuapp.com/posts";

//get
axios
  .get(api_url)
  .then((res) => {
    res.data.map((hasil) => {
      content.innerHTML += `<tr>
          
       <th scope="row" id="nomer" >${hasil.id}</th>
       <td id="title">${hasil.title}</td>
       <td id="content">${hasil.content}</td>
       <td> 
       <button type="button" class="btn btn-danger " id="hapusid" data-index="${hasil.id}"data-bs-toggle="modal" data-bs-target="#delete" >Hapus</button>
       <button type="button" class="btn btn-warning fullstuck" id="editid" data-index="${hasil.id}"  data-bs-toggle="modal" data-bs-target="#edit">Edit</button>
       </td>
       </tr>
    
    
       `;
    });
  })
  .catch((err) => console.log(err));

const modalDelete = document.getElementById("delete");
modalDelete.addEventListener("shown.bs.modal", (e) => {
  const btn = e.relatedTarget;
  const nilai = btn.dataset.index;
  console.log(nilai);
  document
    .getElementById("hapusdata")
    .addEventListener("click", () => hapus(nilai));
});

function hapus(nilai) {
  let titlee = document.querySelector("#edittitle").value;
  let contentt = document.querySelector("#editcontent").value;

  console.log("edit ke excute");
  axios
    .delete(`https://edi-server.herokuapp.com/posts/${nilai}`, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di Edit");
    })
    .then(function (response) {
      location.reload();
    })
    .catch(function (error) {
      console.log(error);
    });
}

const modalEdit = document.getElementById("edit");
modalEdit.addEventListener("shown.bs.modal", (e) => {
  const btn = e.relatedTarget;
  const nilai = btn.dataset.index;
  console.log(nilai);
  document.getElementById("ubahdata").addEventListener("click", function () {
    edit(nilai);
  });
});

function edit(nilai) {
  let titlee = document.querySelector("#edittitle").value;
  let contentt = document.querySelector("#editcontent").value;

  console.log("edit ke excute");
  axios
    .put(`https://edi-server.herokuapp.com/posts/${nilai}`, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di Edit");
    })
    .then(function (response) {
      location.reload();
    })
    .catch(function (error) {
      console.log(error);
    });
}

function tambah() {
  // let title = document.querySelector("#formTitle").value;
  let titlee = document.querySelector("#formTitle").value;
  let contentt = document.querySelector("#formContent").value;

  axios
    .post(api_url, {
      title: titlee,
      content: contentt,
    })
    .then(function (response) {
      alert("Data Berhasil Di tambahkan");
    })
    .then(function (response) {
      location.reload();
    })
    .catch(function (error) {
      console.log(error);
    });
}
// function tambah() {

// window.addEventListener('load', ()=>{

// const form = document.getElementById("form");
// form.addEventListener('submit', (e)=>{
//     e.preventDefault();
//     //creates a multipart/form-data object
//     let data = new FormData(form);

//     axios({
//       method  : 'post',
//       url : api_url,
//       data : new URLSearchParams({
//   title: document.querySelector("#formTitle").value,
//   content: document.querySelector("#formContent").value,
// }),
//       headers: {
//         "Content-Type": "application/x-www-form-urlencoded"
//       }
//     })
//     .then((res)=>{
//       console.log(res);
//     }).then(location.reload())
//     .catch((err) => {throw err});
// });
// });

// }
// tambah()

//Edit / Put Data with axios
