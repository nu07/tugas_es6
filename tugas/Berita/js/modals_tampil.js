doc = document.getElementById("tampilmodal");

async function tampil(id) {
  // const doc = document.getElementById('tampil').getAttribute('index')
  // const slug = doc.dataset.index;

 await axios
    .get(`https://jabar.pojoksatu.id/wp-json/wp/v2/posts/?slug=${id}`)
    .then(function (response) {
      // handle success
      //   console.log(response.data);
      const data = response.data;
      data.map((a) => {
        doc.innerHTML = `


        <div class="modal-header" >
        <h5 class="modal-title" id="exampleModalLabel">${a.title.rendered}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h1 class="text-center">${a.title.rendered}</h1>
      <img src="${a.featured_image_src_medium}" class="rounded mx-auto d-block" alt="..." >
      <p class="mt-4">${a.content.rendered}</p>
      <p> Sumber : ${a.link} </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    
      </div>

      
        
        `;
      });
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
