const konten = document.getElementById("konten");
const d = new Date();
document.getElementById("cuaca").innerHTML += d.getDate() + "-";
document.getElementById("cuaca").innerHTML += d.getUTCMonth() + 1 + "-";
document.getElementById("cuaca").innerHTML += d.getFullYear();
function hasil() {
  axios
    .get("https://jabar.pojoksatu.id/wp-json/wp/v2/posts/?6&per_page=6")
    .then(function (response) {
      // handle success
      const res = response.data;
      // better_featured_image
      // const gambar = res[0].better_featured_image.map((p)=>p.media_details.map((p)=>source_url))

      res.map((hasil) => {
        // console.log(hasil.slug);
        konten.innerHTML += `

<div class="col-sm-6" >
<a href="#tampil" class="text-dark" id="${
          hasil.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);" >
                  <div class="py-3 border-bottom" >
                    <div class="d-flex align-items-center pb-2">
                      <img
                        src="${hasil.better_featured_image.source_url}"
                        class="img-xs img-rounded mr-2"
                        alt="thumb"
                      />
                      <span class="fs-12 text-muted">${hasil.authorname
                        .split("-")
                        .join(" ")}</span>
                    </div>
                    <p class="fs-14 m-0 font-weight-medium line-height-sm">
                    ${hasil.title.rendered.split("-").join(" ")}
                    </p>
                  </div>
                  </a>
                </div>

`;
      });

      // console.log(res);
      // res.map(hasil => {
      //     console.log(hasil.better_featured_image.source_url);
      // })
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
hasil();
//   fetch('https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=6&meta_key=headline&meta_value=1&per_page=3')
//   .then(res => res.json()).then(res => console.log(res))

function headline() {
  axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=6&meta_key=headline&meta_value=1&per_page=1"
    )
    .then(function (response) {
      // handle success
      const res = response.data;
      // console.log(res);
      const doc = document.getElementById("main-banner-carousel");
      res.map((h) => {
        doc.innerHTML += `

<div class="item">
<a href="" class="text-dark" id="${
          h.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
<div class="carousel-content-wrapper mb-2">
  <div class="carousel-content">
    <h1 class="font-weight-bold">
  ${h.better_featured_image.caption}
    </h1>
    <h5 class="font-weight-normal  m-0">
     ${h.title.rendered.split("-").join(" ")}
    </h5>
    <p class="text-color m-0 pt-2 d-flex align-items-center">
      <span class="fs-10 mr-1"></span>
      <i class="mdi mdi-bookmark-outline mr-3"></i>
      <span class="fs-10 mr-1"></span>
      <i class="mdi mdi-comment-outline"></i>
    </p>
  </div>
  <div class="img-fluid" >
    <img src="${
      h.better_featured_image.source_url
    }" alt="" style="max-width:80%; max-height:80%;"/>
  </div>
  </a>
</div>



`;
      });
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
headline();

function header() {
  const kepala = document.getElementById("heeeead");
  axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=6&per_page=4&tags=47462&page=1"
    )
    .then(function (response) {
      // handle success
      const data = response.data;

      data.map((d) => {
        kepala.innerHTML += `
        <a href="" class="text-dark" id="${
          d.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
        <div class="d-flex justify-content-between  mb-3 mb-lg-0">
        
        <div>
          <img
            src="${d.featured_image_src}"
            alt="thumb"
            class="banner-top-thumb"
          />
        </div>
        <h5 class="m-0 font-weight-bold">
         ${d.title.rendered.split("-").join(" ")}
        </h5>
      </div>
      </a>
      `;
      });
      // console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
header();

async function satu() {
  await axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=1&per_page=4&tags=47462&page=1"
    )
    .then(function (response) {
      // handle success
      const ambil = document.getElementById("ngontenn");
      const data = response.data;
      data.map((a) => {
        ambil.innerHTML += `
<a href="" class="text-dark" id="${
          a.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
 
<div class="col-lg-3 col-sm-6 grid-margin mb-5 mb-sm-2">
                <div class="position-relative image-hover">
                  <img
                    src="${a.featured_image_src}"
                    class="img-fluid"
                    alt="world-news"
                  />
                  <span class="thumb-title">${a.authorname}</span>
                </div>
                <h5 class="font-weight-bold mt-3">
                  ${a.better_featured_image.caption.split("-").join(" ")}
                </h5>
                <p class="fs-15 font-weight-normal">
                 ${a.title.rendered.split("-").join(" ")}
                </p>
                <a href="#" class="font-weight-bold text-dark pt-2"
                  >Read Article</a
                >
              </div>
</a>
              `;
      });
      //   console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
satu();

async function populardua() {
  await axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=3&per_page=2&tags=47462&page=1"
    )
    .then(function (response) {
      // handle success
      const form = document.getElementById("kontenn");
      const data = response.data;
      data.map((a) => {
        form.innerHTML += `

<div class="col-sm-6  mb-5 mb-sm-2">
<a href="" class="text-dark" id="${
          a.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
<div class="position-relative image-hover">
  <img
    src="${a.featured_image_src}"
    class="img-fluid"
    alt="world-news"
  />
  <span class="thumb-title">${a.better_featured_image.alt_text
    .split("-")
    .join(" ")}</span>
</div>
<h5 class="font-weight-600 mt-3">
  ${a.title.rendered.split("-").join(" ")}
</h5>
<p class="fs-15 font-weight-normal">
 ${a.better_featured_image.caption}
</p>
</a>
</div>

`;
      });
      // console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
populardua();

async function populartiga() {
  const konten = document.getElementById("kontennnn");
  await axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=4&per_page=2&tags=47462&page=1"
    )
    .then(function (response) {
      // handle success
      const data = response.data;
      data.map((a) => {
        konten.innerHTML += `
    <div class="col-sm-6  mb-5 mb-sm-2">
    <a href="" class="text-dark" id="${
      a.slug
    }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
                    <div class="position-relative image-hover">
                      <img
                        src="${a.featured_image_src}"
                        class="img-fluid"
                        alt="world-news"
                      />
                      <span class="thumb-title">${a.better_featured_image.alt_text
                        .split("-")
                        .join(" ")}</span>
                    </div>
                    <h5 class="font-weight-600 mt-3">
                    ${a.title.rendered.split("-").join(" ")}
                    </h5>
                    <p class="fs-15 font-weight-normal">
                    ${a.better_featured_image.caption.split("-").join(" ")}
                    </p>
                    </a>
                  </div>
    
    `;
      });

      // console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

populartiga();

function gambargede() {
  const konten = document.getElementById("gambargedebanget");
  axios
    .get(
      "https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories=5&per_page=1&tags=47462&page=1"
    )
    .then(function (response) {
      // handle success
      const data = response.data;
      data.map((a) => {
        konten.innerHTML += `
        <a href="" class="text-dark" id="${
          a.slug
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
        <div class="position-relative image-hover">
                  <img
                    src="${a.featured_image_src}"
                    class="img-fluid"
                    alt="world-news"
                  />
                  <span class="thumb-title">NEWS</span>
                </div>
                <h1 class="font-weight-600 mt-3">
                ${a.better_featured_image.alt_text.split("-").join(" ")}
                </h1>
                <p class="fs-15 font-weight-normal">
                ${a.title.rendered.split("-").join(" ")} ${a.excerpt.rendered}
                </p>
       </a>

                `;
      });
      //  (response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}
gambargede();
