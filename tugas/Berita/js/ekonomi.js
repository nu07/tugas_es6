document.getElementById("ekonomis").addEventListener("click", function () {
  ekonomi();
});




function ekonomi() {
    const elem = document.getElementById("pencahariandata");
    elem.innerHTML = "";
    axios.get('https://berita-indo-api.vercel.app/v1/cnn-news/ekonomi')
    .then(function (response) {
      // handle success
      const data = response.data.data
      console.log(data);
      elem.innerHTML = `<div class="row" id="menghasilkan">`;
        const dat = document.getElementById("menghasilkan");

        data.map(a=>{
            dat.innerHTML+= ` 
            <div class="card ml-4" style="width: 18rem;">
  <img class="card-img-top" src="${a.image.large}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">${a.title}</h5>
    <p class="card-text">${a.contentSnippet}</p>
    <a href="${a.link}" target="_blank" class="btn btn-primary">Sumber</a>
  </div>
</div>
            
            `
        })
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });

}


