// 19007
// covid == https://jabar.pojoksatu.id/wp-json/wp/v2/posts?categories19007&meta_key=headline&meta_value=1&per_page=3
// wp/v2/posts?categories=6&meta_key=headline&meta_value=1&per_page=3
// https://jabar.pojoksatu.id/wp-json/wp/v2/posts?search=kesehatan
// 8 =cianjur

document.getElementById("bogors").addEventListener("click", function () {
  bogor();
});

async function bogor() {
  await axios
    .get("https://jabar.pojoksatu.id/wp-json/bogor/v1/popular")
    .then(function (response) {
      const elem = document.getElementById("pencahariandata");
      elem.innerHTML = "";
      // handle success
      const data = response.data.content;
      elem.innerHTML = `<div class="row" id="menghasilkan">`;
      const ambil = document.getElementById("menghasilkan");
      console.log(data);
      data.map((a) => {
        ambil.innerHTML += `

<a href="" class="text-dark" id="${
          a.post_name
        }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">


<div class="col"> 

<div class="card" style="width: 18rem;">
<img class="card-img-top" src="${a.post_thumb}" alt="Card image cap">
<div class="card-body">
<p class="card-text">${a.post_title.split("-").join(" ")}</p>
</div>
</div>

</div>

</a>
`;
      });
      console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

document.getElementById("cianjurs").addEventListener("click", function () {
  bogor();
});

async function cianjur() {
 await axios
    .get("https://jabar.pojoksatu.id/wp-json/cianjur/v1/popular")
    .then(function (response) {
      // handle success
      const elem = document.getElementById("pencahariandata");
      elem.innerHTML = "";
      elem.innerHTML = `<div class="row" id="menghasilkan">`;
      const ambil = document.getElementById("menghasilkan");
      const data = response.data.content;
      console.log(data);
      data.map((a) => {
        ambil.innerHTML += `

      <a href="" class="text-dark" id="${
        a.post_name
      }" data-toggle="modal" data-target="#exampleModal" onclick="tampil(this.id);">
      
      
      <div class="col"> 
      
      <div class="card" style="width: 18rem;">
      <img class="card-img-top" src="${a.post_thumb}" alt="Card image cap">
      <div class="card-body">
      <p class="card-text">${a.post_title.split("-").join(" ")}</p>
      </div>
      </div>
      
      </div>
      
      </a>
      `;
      });
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}

document.getElementById("teknologis").addEventListener("click", function () {
  tekno();
});

// https://berita-indo-api.vercel.app/v1/antara-news/tekno
async function tekno (){
// element utama
const elem = document.getElementById("pencahariandata");
elem.innerHTML = "";
elem.innerHTML = `<div class="row" id="menghasilkan">`;
const ambil = document.getElementById("menghasilkan");
// console.log(data);

// end element utama

await axios.get('https://berita-indo-api.vercel.app/v1/antara-news/tekno')
.then(function (response) {
    const data = response.data.data;
    // handle success
    console.log(data);

    data.map(a=>{
      var d = new Date(a.isoDate);
      ambil.innerHTML+=`
      <a href="${a.link}" class="text-dark" target="_blank">
      <div class="card mb-3">
  <img class="card-img-top" src="${a.enclosure.url}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">${a.title}</h5>
    <p class="card-text">${a.description}</p>
    <p class="card-text"><small class="text-muted">${d.getMinutes()} Menit yang lalu</small></p>
    <hr class="my-4">
    </div>
    </div>
    </a>
         `
    })
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });

}