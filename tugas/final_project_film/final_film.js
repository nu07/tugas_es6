let konten = document.getElementById("content");
let inti = document.getElementById('inti');

const base_url = "https://api.themoviedb.org/3/";
const api_key = "78af6b25d1f9e534634e57f71b23a7f2";
const bahasa = "&language=id";
const image = "https://image.tmdb.org/t/p/w500";
const genre =
  "https://api.themoviedb.org/3/genre/movie/list?api_key=78af6b25d1f9e534634e57f71b23a7f2&language=id";

async function trending() {
  const tren = "trending/all/week?api_key=";
  await axios
    .get(base_url + tren + api_key + "&language=id&region=id")
    .then(function (response) {
      response.data.results.map((film) => {
        konten.innerHTML += `
<div class="card mx-auto mt-5" style="width: 18rem; background-color: black; color: white;">
            <img src="${image}${
          film.poster_path
        }" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">${
                film.original_title || film.original_name || original_title
              } </h5>
              
              <!-- 
               <p class="card-text" data-xyz="value" id="coba">${
                film.overview === ""
                  ? "tidak ada Deskripsi Film Ini"
                  : film.overview
              }</p>
              -->
             
            </div>
            <ul class="list-group list-group-flush" >
              <li class="list-group-item" style="background-color: black; color: white;">Rating Film ini : ${
                film.vote_average === 0 ? "Tidak Ada Rating" : film.vote_average
              }</li>
           
            </ul>
            <div class="card-body">
            <button type="button" class="btn " style="color:white; background-color: #084048;" data-bs-toggle="modal" 
            data-bs-target="#detailfilm" data-id=${film.id}>
            Deskripsi Film
          </button>
          
          
            </div>
          </div>
`;

        // console.log(film);
      });

      // console.log(response.data.results);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
}


trending();


// cari()



const detail = document.getElementById("detailfilm");
detail.addEventListener("shown.bs.modal", async (e) => {
  const btn = e.relatedTarget;
  const nilai = btn.dataset.id;
  const has = btn.dataset.xyz;
console.log(nilai);
 await axios.get(`https://api.themoviedb.org/3/movie/${nilai}?api_key=78af6b25d1f9e534634e57f71b23a7f2&language=id`)
  .then(function (response) {
    // handle success
    const gen = response.data.genres.map(p => p.name)
detail.innerHTML =
`
<div class="modal-dialog" style=" background-color: #231D1C;>
<div class="modal-content" >
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel" style="color:white";>${response.data.original_title}</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">
   
<div id="carrrrrdd">

<div class="card" style="width: 15rem; ">
  <img src="${image}${response.data.poster_path}" class="card-img-top" alt="...">
  <div class="card-body" style=" background-color: #231D1C;">
  
    <p class="card-text" id="genre" style="color: white;"  >Genre : ${gen.join(', ')}</p>
    <p class="card-text" style="color: white;">${response.data.adult == true ? "Film Dewasa!!!" : ""}</p>
    <p class="card-text" style="color: white;">${response.data.overview!== null && response.data.overview !=="" ? response.data.overview : "Tidak Ada Deskripsi Film Ini"}</p>
    <p class="card-text" style="color: white;">Budget Film ${response.data.budget==0 ? "Dana Tidak Di Ketahui" : response.data.budget +'$'}</p>
  </div>
</div>


</div>





  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
 
  </div>
</div>
</div>
</div>
`
    console.log(response.data);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });


  // const valu = btn.dataset.value;
  console.log(nilai);

  
  // https://api.themoviedb.org/3/movie/84958?api_key=78af6b25d1f9e534634e57f71b23a7f2&language=id&region=id

  

})

function yukcari (){
  let pencaharian = document.querySelector("#cari").value;

  axios.get(`https://api.themoviedb.org/3/search/collection?api_key=78af6b25d1f9e534634e57f71b23a7f2&language=en-US&query=${pencaharian}&page=1`)
    .then(function (response) {
      // handle success
      console.log(response);
      if (response.data.total_results == 0) {
        konten.innerHTML =""
          konten.innerText += "Data Tidak Tersedia"
        alert('Data Tidak Tersedia')
      }else {
        alert('gaada benernya')
        konten.innerHTML =""
      }
      response.data.results.map((nyari)=>{
  console.log(nyari.name);
  
  konten.innerHTML+=  ` <div class="card mx-auto mt-5" style="width: 18rem; background-color: black; color: white;">
  <img src="${nyari.poster_path === null ? "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png" : image+nyari.poster_path }" alt="Foto Film" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">${
      nyari.original_title || nyari.original_name || original_title
    } </h5>
    
    <!-- 
     <p class="card-text" data-xyz="value" id="coba">${
      nyari.overview === ""
        ? "tidak ada Deskripsi Film Ini"
        : nyari.overview
    }</p>
    -->
   
  </div>
  <ul class="list-group list-group-flush" >
    <li class="list-group-item" style="background-color: black; color: white;">Rating Film ini : ${
      nyari.vote_average === 0 || undefined ||null? "Tidak Ada Rating" : nyari.vote_average
    }</li>
 
  </ul>
  <div class="card-body">
  <button type="button" class="btn " style="color:white; background-color: #084048;" data-bs-toggle="modal" data-bs-target="#detailfilm" data-id=${nyari.id}>
  Deskripsi Film
</button>


  </div>
</div>
  
  
  
  `})
// })  
      // console.log(response)
      alert("sukses")
    })
    .catch(function (error) {
      // handle error
      alert('eror')
      console.log(error);
    })
    .then(function () {
      // always executed
    });

}


